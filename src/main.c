#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "r-aux.h"
#include "armtimer.h"
#include "gpio.h"
#include "interrupts.h"
#include "systimer.h"

void _enable_interrupts();

/** Main function - we'll never return from here */
void kernel_main( unsigned int r0, unsigned int r1, unsigned int atags )
{
    // Set GPIO pin for the LED to output
    RPI_GetGpio() -> LED_GPFSEL |= LED_GPFBIT;

    // Enable the timer interrupt IRQ 
    RPI_GetIrqController() -> Enable_Basic_IRQs = RPI_BASIC_ARM_TIMER_IRQ;

    // Setup the system timer interrupt 
    // Timer frequency = Clk/256 * 0x400 
    RPI_GetArmTimer() -> Load = 0x400;

    // Configure ARM timer 
    RPI_GetArmTimer()->Control =
            RPI_ARMTIMER_CTRL_23BIT |
            RPI_ARMTIMER_CTRL_ENABLE |
            RPI_ARMTIMER_CTRL_INT_ENABLE |
            RPI_ARMTIMER_CTRL_PRESCALE_256;

    // Enable interrupts
    _enable_interrupts();

    // Initialize UART
    RPI_AuxMiniUartInit( 115200, 8 );
    printf("Hello world!");

    // Trap
    while(1)
    {

    }
}