#ifndef ARMTIMER_H
#define ARMTIMER_H

	#include <stdint.h>
	#include "base.h"

	// Base address of the timer peripheal
	#define RPI_ARMTIMER_BASE               ( PERIPHERAL_BASE + 0xB400 )
	
	// Define masks for timer options
	// 	Counter size
	#define RPI_ARMTIMER_CTRL_16BIT         ( 0 << 1 )
	#define RPI_ARMTIMER_CTRL_23BIT         ( 1 << 1 )
	
	//		Prescale
	#define RPI_ARMTIMER_CTRL_PRESCALE_1    ( 0 << 2 )
	#define RPI_ARMTIMER_CTRL_PRESCALE_16   ( 1 << 2 )
	#define RPI_ARMTIMER_CTRL_PRESCALE_256  ( 2 << 2 )

	//		Timer interrupt enable
	#define RPI_ARMTIMER_CTRL_INT_ENABLE    ( 1 << 5 )
	#define RPI_ARMTIMER_CTRL_INT_DISABLE   ( 0 << 5 )

	//		Timer enable
	#define RPI_ARMTIMER_CTRL_ENABLE        ( 1 << 7 )
	#define RPI_ARMTIMER_CTRL_DISABLE       ( 0 << 7 )


	// ARM timer state & configuration
	typedef struct 
	{
	    /* The value the timer counter is set to either at the beginning or when
	    	 it has reached 0. Writing to this register causes and immediate reload
	    	 of the timer. */
	    volatile uint32_t Load;

	    /* The current value of the timer counter */
	    volatile uint32_t Value;

	    /* Bits 0 - 9 are used to configure the timer */
	    volatile uint32_t Control;

	    /* When this register is written to, it clears the IRQ flag*/
	    volatile uint32_t IRQClear;

	    /* The status of the IRQ flag
	       0 : The interrupt pending bits is clear.
	       1 : The interrupt pending bit is set.*/
	    volatile uint32_t RAWIRQ;

	    /* Logical AND of the IRQ and its enable mask
	       0 : Interrupt line not asserted. 
	       1 : Interrupt pending and the interrupt enable bit are set */
	    volatile uint32_t MaskedIRQ;

	    /* Similar to the Load register, however it does not cause an immediate
	    	 reload. The timer count is only reset to this value after it has reached 0.*/
	    volatile uint32_t Reload;

	    /* 10 bit pre-divider register.
	    	 timer_clock = apb_clock/(pre_divider + 1)
	       The reset value of this register is 0x7D so gives a divide by 126. */
	    volatile uint32_t PreDivider;

	    /* The free running counter is a 32 bits wide read only register. 
	    	 The register is enabled by setting bit 9 of the Timer control register. 
	    	 The free running counter is incremented immediately after it is enabled.
	    	 The timer can not be reset but when enabled, will always increment and 
	    	 roll-over. The free running counter is also running from the APB clock 
	    	 and has its own clock pre-divider controlled by bits 16-23 of the timer
	       control register. This register will be halted too if bit 8 of the control 
	       register is set and the ARM is in Debug Halt mode. */
	    volatile uint32_t FreeRunningCounter;

	    } rpi_arm_timer_t;


	rpi_arm_timer_t* RPI_GetArmTimer(void);
	void RPI_ArmTimerInit(void);

#endif