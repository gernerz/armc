#include <stdint.h>
#include <stdbool.h>

#include "armtimer.h"
#include "base.h"
#include "gpio.h"
#include "interrupts.h"

// Base interrupt controller
static rpi_irq_controller_t* rpiIRQController = 
		 (rpi_irq_controller_t*)RPI_INTERRUPT_CONTROLLER_BASE;

rpi_irq_controller_t* RPI_GetIrqController( void )
{
	return rpiIRQController;
}

/* The Reset vector interrupt handler
   This can never be called, since an ARM core reset would also reset the
   GPU and therefore cause the GPU to start running code again until
   the ARM is handed control at the end of boot loading */
void __attribute__((interrupt("ABORT"))) reset_vector(void)
{ }

/* The undefined instruction interrupt handler. Trap if ocurrs. */
void __attribute__((interrupt("UNDEF"))) undefined_instruction_vector(void)
{
	while( 1 )
	{
	  /* Do Nothing! */
	}
}

/* The supervisor call interrupt handler. */
void __attribute__((interrupt("SWI"))) software_interrupt_vector(void)
{
	while( 1 )
	{
	  /* Do Nothing! */
	}
}


/* The prefetch abort interrupt handler. */
void __attribute__((interrupt("ABORT"))) prefetch_abort_vector(void)
{
	while( 1 )
	{
	  /* Do Nothing! */
	}
}


/* The Data Abort interrupt handler. */
void __attribute__((interrupt("ABORT"))) data_abort_vector(void)
{
	while( 1 )
	{
	  /* Do Nothing! */
	}
}


/* The IRQ Interrupt handler
   This handler is run every time an interrupt source is triggered.
	Ensure the interrupt flag is cleared before returning.
*/
void __attribute__((interrupt("IRQ"))) interrupt_vector(void)
{
	static int lit = 0;

	RPI_GetArmTimer()->IRQClear = 1;

	/* Flip the LED */
	if( lit )
	{
	  LED_OFF();
	  lit = 0;
	}
	else
	{
	  LED_ON();
	  lit = 1;
	}
}

/**
    @brief The FIQ Interrupt Handler
    The FIQ handler can only be allocated to one interrupt source. The FIQ has
    a full CPU shadow register set. Upon entry to this function the CPU
    switches to the shadow register set so that there is no need to save
    registers before using them in the interrupt.
    In C you can't see the difference between the IRQ and the FIQ interrupt
    handlers except for the FIQ knowing it's source of interrupt as there can
    only be one source, but the prologue and epilogue code is quite different.
    It's much faster on the FIQ interrupt handler.
    The prologue is the code that the compiler inserts at the start of the
    function, if you like, think of the opening curly brace of the function as
    being the prologue code. For the FIQ interrupt handler this is nearly
    empty because the CPU has switched to a fresh set of registers, there's
    nothing we need to save.
    The epilogue is the code that the compiler inserts at the end of the
    function, if you like, think of the closing curly brace of the function as
    being the epilogue code. For the FIQ interrupt handler this is nearly
    empty because the CPU has switched to a fresh set of registers and so has
    not altered the main set of registers.
*/
void __attribute__((interrupt("FIQ"))) fast_interrupt_vector(void)
{

}