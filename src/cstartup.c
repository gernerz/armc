// These are defined in the linker script rpi.x
extern int __bss_start__;
extern int __bss_end__;

// The main function is defined in the linker script rpi.x
extern void kernel_main( unsigned int r0, unsigned int r1, unsigned int atags );

void _cstartup( unsigned int r0, unsigned int r1, unsigned int r2 )
{
	int* bss = &__bss_start__;
	int* bss_end = &__bss_end__;

	// Initialize bss data to 0
	while( bss < bss_end )
	{
		*bss++ = 0;
	}

	// Main should never be return from.
	kernel_main(r0, r1, r2);

	// Safely trap execution here if main is accidentally returned from.
	while(1)
	{
		// Block Forever
	}
}